#!/bin/sh
ffmpeg -video_size 1440x900 -framerate 60 -f x11grab -i :0.0 -c:v libx264 -preset veryfast [file].mkv &
aucat -f snd/mon -o [file].wav &
